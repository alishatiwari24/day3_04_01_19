import java.util.*;
class LRUCache
{
	public static void main(String args[])
	{
//LRUCache with size=5
		Queue<Integer> LRUQueue=new LinkedList<Integer>();
		Scanner sc=new Scanner(System.in);
		int ele,size;
		char ch='y';
		
		while(ch=='y' || ch=='Y')
		{
			System.out.println("Do you want to add new process?");
			ch=sc.next().charAt(0);
			if(ch=='n' || ch=='N')
				break;
			
//Adding a new process in LRUCaches
			System.out.println("Enter id of new process");
			ele=sc.nextInt();
			size=LRUQueue.size();
			if(size==5)
			{
				LRUQueue.remove();
				System.out.println("Process " + (LRUQueue.element()-1) + " Removed from LRU Queue!!!");
			}
			LRUQueue.add(ele);
		}
		System.out.println("Currently running processes!!! ");
		System.out.println(LRUQueue);
	}
}