import java.util.*;

import com.sun.javafx.collections.SetAdapterChange;
//Node operations
class Node
{
	public int data;
	public Node addr;
	Node()
	{
		data=0;
		addr=null;
	}
	Node(int d,Node ad)
	{
		data=d;
		addr=ad;
	}
	public Node getAddr()
	{
		return addr;
	}
	public int getData()
	{
		return data;
	}
	public void setData(int d)
	{
		data=d;
	}
	public void setAddr(Node ad)
	{
		addr=ad;
	}
}
//Whole linkedlist operations
class LinkedList
{
	public Node first;
	public Node last;
	public int size;
	LinkedList()
	{
		first=null;
		last=null;
		size=0;
	}
	public int getSize()
	{
		return size;
	}
	
//Insert at Begin
	public void insertFirst(int d)
	{
		Node nd=new Node(d,null);
		size++;
		if(first==null)
		{
			first=nd;
			last=first;
		}
		else
		{
			nd.setAddr(first);
			first=nd;
		}
	}
	
//Insert At End
	public void insertLast(int d)
	{
		Node nd=new Node(d,null);
		if(first==null)
		{
			first=nd;
			last=first;
		}
		else
		{
			last.setAddr(nd);
			size++;
			last=nd;
		}
	}
	
//Insert at Specific Position
	public void insertAtPos(int d,int pos)
	{
		Node nd=new Node(d,null);
		int prev=pos-1;
		Node ptr=first;
		for(int i=1;i<size;i++)
		{
			if(i==prev)
			{
				Node temp=ptr.getAddr();
				ptr.setAddr(nd);
				nd.setAddr(temp);
				//nd.setData(d);
				//ptr.setAddr(nd);
				size++;
				break;
			}
			ptr=ptr.getAddr();
		}
	}

//Delete From Begin
	public void deleteFirst()
	{
		Node temp=first.getAddr();
		first=temp;
		size--;
	}
//Delete From Last
	public void deleteLast()
	{
		Node ptr=first;
		
		for(int i=1;i<size-1;i++)
		{
			ptr=ptr.getAddr();
			
		}
		ptr.setAddr(null);
		size--;
	}
	
//Delete From Specific Location
	public void deleteFromPos(int pos)
	{
		Node ptr=first;
		int prev=pos-1;
		//int next=pos+1;
		for(int i=1;i<size-1;i++)
		{
			if(i==prev)
			{
				Node temp=ptr.getAddr();
				temp=temp.getAddr();
				ptr.setAddr(temp);
			}
			ptr=ptr.getAddr();
		}
		size--;
	}


//Display Linked List 
	public void display()
	{
		
		if(size==0)
		{
			System.out.println("Linked List is Empty!!!");
		}
		else if(size==1)
		{
			System.out.println(first.getData() + " | " + first.getAddr());
		}
		else
		{
			System.out.println("Linked List is !!!");
			Node ptr=first;
		
			System.out.println(first.getData() + " | " + first.getAddr());
			ptr=first.getAddr();
			while(ptr.getAddr()!=null)
			{
				System.out.println(ptr.getData() + " | " + ptr.getAddr());
				ptr=ptr.getAddr();
			}
			System.out.println(ptr.getData() + " | " + ptr.getAddr());
		}
	}
}
class LinkedListDemo
{
	public static void main(String args[]) 
	{ 
		Scanner sc=new Scanner(System.in);
		LinkedList ll=new LinkedList();
		
		int data,pos, option;
		
		char ch;
		do
		{
			System.out.println("Select an Option!!! \n1.Insert At Begin \n2.Insert At End \n3.Insert At Specific Position \n4.Delete From Begin \n5.Delete From Specific Position \n6.Display LinkedList 7.\nWrite your choice?");
			option=sc.nextInt();
				switch(option)
				{
				case 1: System.out.println("Enter the data?");
						data=sc.nextInt();
						ll.insertFirst(data);
						break;
				case 2: System.out.println("Enter the data?");
						data=sc.nextInt();
						ll.insertLast(data);
						break;
				case 3: System.out.println("Enter the position?");
						pos=sc.nextInt();
						System.out.println("Enter the data?");
						data=sc.nextInt();
						ll.insertAtPos(data, pos);
						break;
				case 4: ll.deleteFirst();
						System.out.println("Data at First postion is Deleted!!!");
						break;
				case 5: System.out.println("Enter position to delete from?");
						pos=sc.nextInt();
						ll.deleteFromPos(pos);
						System.out.println("Data Deleted Successfully!!!");
						break;
				case 6: ll.deleteLast();
						break;
				case 7: ll.display();
						int size=ll.getSize();
						System.out.println("Size = " + size);
						break;
				default: System.out.println("Please Enter a Correct Option!!!");
				
				}
				System.out.println("Do you want to quit? y/n");
				ch=sc.next().charAt(0);
		}while(ch=='n' || ch=='N');
	}
}