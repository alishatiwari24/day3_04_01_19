import java.util.*;

class CollectionsDemo
{
	public static void main(String args[]) throws Exception
	{
		long starting=System.currentTimeMillis();
//Inserting values
		ArrayList<Integer> arrayint=new ArrayList<Integer>();
		for(int i=1;i<=100000;i++)
		{
			arrayint.add(i-1,i);
		}
		Scanner sc=new Scanner(System.in);
		
		char ch;
		try
		{
			do
			{
				System.out.println("1.Update \n2.Delete \n3.Print \nSelect an Option?");
				int option=sc.nextInt();
				switch(option)
				{
					case 1://Update code	
							int index,value;
							System.out.println("Enter index to Update value?");
							index=sc.nextInt();
							
							//arrayint.remove(index);
							System.out.println("Enter value for index = " + index + "?");
							value=sc.nextInt();
							//arrayint.add(index-1,value);
							arrayint.set(index-1,value);
							System.out.println("Update done : [" + index +"] = " +value);
							break;
					case 2://Delete code	
							System.out.println("Enter index to be removed?");
							index=sc.nextInt();
							
							arrayint.remove(index-1);
							System.out.println("Deletion done : [" + index + "] removed!!!");
							break;
					case 3://Printing List
						
							System.out.println("Total no. of Elements = " + arrayint.size());
							Iterator<Integer> itr=arrayint.iterator();
							int i=1;
							
							while(itr.hasNext())
							{
								System.out.println("["+i+"]" + " = " + itr.next());
								i++;
							}
							break;
					default:System.out.println("Choose appropriate option!!!");
				}
				//Execution Time
				long ending=System.currentTimeMillis();
				System.out.println("Execution Time = " + (ending-starting) + "ms");
				
				System.out.println("Do you want to quit? y/n");
				ch=sc.next().charAt(0);
			}while(ch=='n' || ch=='N');
		}
		catch(Exception e)
		{
			System.out.println("ERROR: Please enter index between 1-10000!!!");
		}
	}
}