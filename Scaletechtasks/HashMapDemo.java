import java.util.*;
class HashMapDemo
{
	public static void main(String args[])
	{
		long starting=System.currentTimeMillis();
//Inserting values
		HashMap<Integer,Integer> mapint=new HashMap<Integer,Integer>();
		for(int i=1;i<=10000;i++)
		{
			mapint.put(i,i);
		}
//Update a value
		int key,value;
		System.out.println("Enter key of value to update?");
		Scanner sc=new Scanner(System.in);
		key=sc.nextInt();
		System.out.println("Enter value for key " + key);
		value=sc.nextInt();
		mapint.replace(key,value);
		System.out.println("Update done : " + key + " = " + value);
		System.out.println("");
//Deleting a value
		System.out.println("Enter key of value to delete?");
		key=sc.nextInt();
		mapint.remove(key);
		System.out.println("Deletion  done successfully!!!");
//Printing Map
		System.out.println(mapint);
		
		long ending=System.currentTimeMillis();
		System.out.println("Execution Time = " + (ending-starting) + " ms");
	}
}